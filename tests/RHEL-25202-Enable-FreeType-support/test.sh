#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
. /usr/share/beakerlib/beakerlib.sh || exit 1

rlJournalStart
    rlPhaseStartSetup
        rlRun "tmp=\$(mktemp -d)" 0 "Create tmp directory"
        rlRun "cp tk-test.sh tk-test.pl $tmp"
        rlRun "pushd $tmp"
        rlVirtualXStart "tk"
        export DISPLAY="$( rlVirtualXGetDisplay \"tk\" )"
    rlPhaseEnd

    rlPhaseStartTest
        # Get output from tcl/Tk test script
	rlRun "./tk-test.sh > tcl-output.log" 0 "Running tcl/Tk test script'"

	# Get output from perl-Tk test script
	rlRun "./tk-test.pl > perl-output.log" 0 "Running perl-Tk test script'"

	# Compare outputs
	rlAssertNotDiffer "tcl-output.log" "perl-output.log"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlVirtualXStop "tk"
        rlRun "popd"
        rlRun "rm -r $tmp" 0 "Remove tmp directory"
    rlPhaseEnd
rlJournalEnd
